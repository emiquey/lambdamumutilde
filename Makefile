all:
	coqc -R . "L" L_Core_Definitions.v
	coqc -R . "L" L_Core_Infrastructure.v
	coqc -R . "L" L_Core_Soundness.v
	coqc -R . "L" L_Core_Adequacy.v

